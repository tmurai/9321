package edu.unsw.comp9321.bookstore;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;

/**
 * Assign 1 instructions:
 * 
 * The operations (actions) in the JSP files are directed to the doGet() or doPost()
 * methods in this class.
 *  
 * Use RequestDispatcher.forward(), and request and session attributes to transfer control
 * and data between this servlet and JSPs.
 * 
 */
public class ControlServlet extends HttpServlet {
	BookList books;
	Cart shoppingCart;
	
	public ControlServlet() {
		super();
		shoppingCart = new Cart();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) { 
		try {
			if (books == null) {
				books = readBooks(getServletContext());
			}
			ServletContext context = getServletContext();
			String req = request.getParameter("op");
			
			if (req != null) {
				if (req.contains("callSearch")) {
					response.sendRedirect("search.jsp");
				} else if (req.contains("search")) {
					HttpSession session = request.getSession(true);
					BookList searchList = searchBooks(request, books.getBooks());
					session.setAttribute("searchList", searchList);
					RequestDispatcher rd = context.getRequestDispatcher("/searchResult.jsp");
					rd.forward(request, response);
				} else if (req.contains("info")) {
					String title = request.getParameter("title");
					HttpSession session = request.getSession(true);
					session.setAttribute("book", books.searchBooks(title));
					RequestDispatcher rd = context.getRequestDispatcher("/bookInfo.jsp");
					rd.forward(request, response);
				} else if (req.contains("addCart")) {
					addToCart(request);
					HttpSession session = request.getSession(true);
					session.setAttribute("cart", shoppingCart);
					RequestDispatcher rd = context.getRequestDispatcher("/reviewCart.jsp");
					rd.forward(request, response);
				} else if (req.contains("reviewCart")) {
					String remove = request.getParameter("remove");
					if (remove != null) {
						removeFromCart(request);
						HttpSession session = request.getSession(true);
						session.setAttribute("cart", shoppingCart);
						RequestDispatcher rd = context.getRequestDispatcher("/reviewCart.jsp");
						rd.forward(request, response);
					}
					String checkout = request.getParameter("checkout");
					if (checkout != null) {
						HttpSession session = request.getSession(true);
						session.setAttribute("cart", shoppingCart);
						RequestDispatcher rd = context.getRequestDispatcher("/checkout.jsp");
						rd.forward(request, response);
					}
				} else if (req.contains("checkout")) {
					if (request.getParameter("checkout_yes") != null) {
						HttpSession session = request.getSession(true);
						session.setAttribute("cart", shoppingCart);
						RequestDispatcher rd = context.getRequestDispatcher("/confirm.jsp");
						rd.forward(request, response);
					} else if (request.getParameter("checkout_no") != null) {
						response.sendRedirect("welcome.jsp");
					}
				} else if(req.contains("welcome")) {
					response.sendRedirect("welcome.jsp");
				}
			} else {
				response.sendRedirect("error.jsp");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void removeFromCart(HttpServletRequest request) {
		String[] delete = request.getParameterValues("delete");
		
		if (delete != null) {
			for (String s : delete) {
				Book b = shoppingCart.searchBooks(s);
				shoppingCart.getBooks().remove(b);
			}
		}
	}
	
	private void addToCart(HttpServletRequest request) {
		String[] queryTitles = request.getParameterValues("bookCheckBox");

		ArrayList<Book> list = new ArrayList<Book>();
		if (queryTitles != null) {
			for (String s : queryTitles) {
				Book b = books.searchBooks(s);
				if (b != null) {
					list.add(b);
				}
			}
		}
		shoppingCart.setBooks(list);
	}
	
	private BookList searchBooks (HttpServletRequest request, ArrayList<Book> b) {
		String queryTitle = request.getParameter("title");
		String queryAuthor= request.getParameter("author");
		String queryGenre = request.getParameter("genre");
		ArrayList<Book> books = new ArrayList<Book>();
		
		if (queryTitle != null && queryTitle != "") {
			for (Book book : b) {
				if (book.getTitle().toLowerCase().contains(queryTitle.toLowerCase())) {
					books.add(book);
				}
			}
		}

		if (queryAuthor != null && queryAuthor != "") {
			for (Book book : b) {
				if (book.getAuthor().toLowerCase().contains(queryAuthor.toLowerCase())) {
					if (!books.contains(book)) {
						books.add(book);
					}
				}
			}
		}

		if (queryGenre != null && queryGenre != "") {
			for (Book book : b) {
				if (book.getGenre().toLowerCase().contains(queryGenre.toLowerCase())) {
					if (!books.contains(book)) {
						books.add(book);
					}
				}
			}
		}
		
		BookList bl = new BookList();
		bl.setBooks(books);
		return bl;
	}
	
	private BookList readBooks(ServletContext sc) {
		BookList ret = new BookList();
		
		
		// get data from xml
		try {
			DocumentBuilderFactory factory;
			DocumentBuilder parser;
			Document doc;
			ServletContext context = sc;
			InputStream is = context.getResourceAsStream("/WEB-INF/Books.xml");
			factory = DocumentBuilderFactory.newInstance();

			parser = factory.newDocumentBuilder();

			doc = parser.parse(is);

			NodeList listOfBooks = doc.getElementsByTagName("tns:Entry");
			ArrayList<Book> bookList = new ArrayList<Book>();
			for(int s = 0; s < listOfBooks.getLength() ; s++){
				Node firstBookNode = listOfBooks.item(s);
				
				if(firstBookNode.getNodeType() == Node.ELEMENT_NODE){
					Book b = new Book();
					
					Element bookElement = (Element)firstBookNode;
					// title
					NodeList nodeList = bookElement.getElementsByTagName("tns:Title");
					Element nodeElement = (Element)nodeList.item(0);
					NodeList nodeTitleList = nodeElement.getChildNodes();
					
					b.setTitle(((Node)nodeTitleList.item(0)).getNodeValue().trim());
					
					
					// author(s)
					nodeList = bookElement.getElementsByTagName("tns:Author");
					for (int i = 0 ; i < nodeList.getLength() ; i++) {
						NodeList al = nodeList.item(i).getChildNodes();
						b.setAuthor(al.item(1).getTextContent() + 
								" " + al.item(3).getTextContent());
					}
					
					// genre(s)
					nodeList = bookElement.getElementsByTagName("tns:Genre");
					for (int i = 0 ; i < nodeList.getLength() ; i++) {
						NodeList al = nodeList.item(i).getChildNodes();
						for (int j = 0 ; j < al.getLength() ; j++) {
							b.setGenre(al.item(j).getTextContent());
						}
					}
					
					// year
					nodeList = bookElement.getElementsByTagName("tns:Year");
					nodeElement = (Element)nodeList.item(0);
					nodeTitleList = nodeElement.getChildNodes();
					
					b.setYear(Integer.parseInt(
							((Node)nodeTitleList.item(0)).getNodeValue().trim()));

					// series
					nodeList = bookElement.getElementsByTagName("tns:Series");
					nodeElement = (Element)nodeList.item(0);
					nodeTitleList = nodeElement.getChildNodes();
					if (nodeTitleList.getLength() > 0) {
						b.setSeries(((Node)nodeTitleList.item(0)).getNodeValue().trim());
					} else {
						b.setSeries("");
					}

					// edition
					nodeList = bookElement.getElementsByTagName("tns:Edition");
					nodeElement = (Element)nodeList.item(0);
					nodeTitleList = nodeElement.getChildNodes();
					if (nodeTitleList.getLength() > 0) {
						b.setEdition(Integer.parseInt(
								((Node)nodeTitleList.item(0)).getNodeValue().trim()));
					} else {
						b.setEdition(-1);
					}

					// publisher
					nodeList = bookElement.getElementsByTagName("tns:Publisher");
					nodeElement = (Element)nodeList.item(0);
					nodeTitleList = nodeElement.getChildNodes();
					if (nodeTitleList.getLength() > 0) {
						b.setPublisher(
								((Node)nodeTitleList.item(0)).getNodeValue().trim());
					} else {
						b.setPublisher("");
					}
					
					// price
					nodeList = bookElement.getElementsByTagName("tns:Price");
					nodeElement = (Element)nodeList.item(0);
					nodeTitleList = nodeElement.getChildNodes();
					
					b.setPrice(Double.parseDouble(
							((Node)nodeTitleList.item(0)).getNodeValue().trim()));

					bookList.add(b);
				}
			}
			
			ret.setBooks(bookList);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
}