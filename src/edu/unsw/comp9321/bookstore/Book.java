package edu.unsw.comp9321.bookstore;

import java.io.Serializable;

public class Book implements Serializable{
	private String title;
	private String author;
	private String genre;
	private int year;
	private String series;
	private int edition;
	private String publisher;
	private double price;
	
	public Book() {
		title = "";
		author = "";
		genre = "";
		year = 0;
		series = "";
		edition = 1;
		publisher = "";
		price = 0;
	}
	
	public void setTitle (String t) {
		title = t;
	}
	
	public String getTitle () { return title; }
	
	public void setAuthor (String t) {
		if (author.length() == 0) {
			author = t;
		} else {
			author = author + ", " + t;
		}
	}
	
	public String getAuthor () { return author; }
	
	public void setGenre (String t) {
		if (genre.length() == 0) {
			genre = t;
		} else {
			genre = genre + ", " + t;
		}
	}
	
	public String getGenre () { return genre; }
	
	public void setYear (int i) {
		year = i;
	}
	
	public int getYear () { return year; }
	
	public void setSeries (String t) {
		series = t;
	}
	
	public String getSeries () { return series; }
	
	public void setEdition (int i) {
		edition = i;
	}
	
	public int getEdition () { return edition; }
	
	public void setPublisher (String t) {
		publisher = t;
	}
	
	public String getPublisher () { return publisher; }
	
	public void setPrice (double d) {
		price = d;
	}
	
	public double getPrice () { return price; }
}
