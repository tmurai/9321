package edu.unsw.comp9321.bookstore;

import java.util.ArrayList;

public class Cart extends BookList{
	private ArrayList<Book> books;
	private double sum;
	
	public Cart() {
		books = new ArrayList<Book>();
		sum = 0.0;
	}
	
	public void setBooks(ArrayList<Book> b) {
		books = b;
	}
	
	public ArrayList<Book> getBooks() { return books; }
	
	public Book searchBooks (String title) {
		title = title.trim();
		for(Book book : books) {
			if (book.getTitle().compareTo(title) == 0) 
				return book;
		}
		
		return null;
	}
	
	public double getSum () { 
		sum = 0.0;
		for (int i = 0 ; i < books.size() ; i++) {
			sum += books.get(i).getPrice();
		}
		return sum; 
	}
}
