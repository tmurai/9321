package edu.unsw.comp9321.bookstore;

import java.io.Serializable;
import java.util.ArrayList;

public class BookList implements Serializable {
	protected ArrayList<Book> books;
	
	public BookList() {
		books = new ArrayList<Book>();
	}
	
	public void setBooks(ArrayList<Book> b) {
		books = b;
	}
	
	public ArrayList<Book> getBooks() { return books; }
	
	public Book searchBooks (String title) {
		for(Book book : books) {
			if (book.getTitle().compareTo(title) == 0) 
				return book;
		}
		
		return null;
	}
}
