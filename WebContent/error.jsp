<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!--
	Assign 1 instructions:
	
	If a invalid operation is invoked on the site, then the user is redirected to this page. 
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Error</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="pagehdr">
			<h1>Food for Thought Bookstore</h1>
		</div>
		<h2>Sorry, the operation could not be performed!</h2>
	</body>
</html>