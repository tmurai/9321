<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!--
	Assign 1 instruction:
	This page presents the user with three fields (title, author and genre) 
	for entering search terms. The search function is substring match and is 
	OR of the input fields. 
	There is no need to modify this page as well.
	
	When the user hits the submit button, he/she is taken to searchResult.jsp   
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Search Book</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="pagehdr">
			<h1>Food for Thought Bookstore</h1>
		</div>
		<h2>Please use the following form for search!</h2>	
		<form action="controller">
			<input type="hidden" name="op" value="search"/>
			<strong>Title</strong>: <INPUT NAME="title" TYPE="text"/> <br/>
			<strong>Author</strong>: <INPUT NAME="author" TYPE="text"/> <br/>
			<strong>Genre</strong>: <INPUT NAME="genre" TYPE="text" /> <br/>
			<INPUT TYPE="submit" VALUE="Submit">
		</FORM>
	</body>
</html>