<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!--
	Assign 1 instructions:
	
	This page presents a list of books that are currently in the user's shopping cart.
	
	The list is presented in a table. Each row has the book price, book title and a checkbox. 
	User can select all the books he/she wants to remove from the cart. The book title is NOT
	linked to the bookInfo page.
	
	User clicks on the remove button to remove the checked items from the cart and reloads the reviewCart.jsp page via the controller.
	 
	If the cart is empty, the reviewCart page shows the message "Shopping cart is empty" and presents a link to the search page (via controller)
	
	User clicks on the Checkout button and is taken to the checkout page via the controller.
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Cart</title>
<link href="CSS.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="pagehdr">
		<h1>Food for Thought Bookstore</h1>
	</div>
	<form name="reviewCart" action="controller?op=reviewCart" method="POST">
		<h2>Shopping Cart</h2>
		<c:if test="${cart.books == null}">
			
			</c:if>
		<table border="0">
			<tr>
				<th>Book Title</th>
				<th>Book Price</th>
				<th>Delete ?</th>
			</tr>
			<c:if test="${cart.books != null}">
				<c:if test="${empty cart.books}">
					<p>Shopping cart empty.</p>
					Please <a href="controller?op=callSearch">click here</a> to search for books.
				</c:if>
				<c:if test="${not empty cart.books}">
				<c:forEach var="book" items="${cart.books}">
					<tr>
						<td>${book.title}</td>
						<td><fmt:formatNumber type="number" 
							minFractionDigits="2" maxFractionDigits="2" 
							value="${book.price}" /></td>
						<td><input type="checkbox" name="delete"
							value="${book.title}" /></td>
					</tr>
				</c:forEach>
				</c:if>
			</c:if>

			<!--  List of books in cart. Example of an item:
			<tr>
						<td>The Ancient Roman World</td>
						<td>32.95</td> 
						<td><input type="checkbox" name={Your Parameter Name} value={Your Parameter Value}/></td>
			</tr> 
			-->
			<tr>
				<td>Total Cost</td>
				<td><fmt:formatNumber type="number" 
							minFractionDigits="2" maxFractionDigits="2" 
							value="${cart.sum}" /></td>
			</tr>
		</table>
		<input type="submit" title="Remove from cart" name="remove"
			value="remove" /> <input type="submit" title="Checkout"
			name="checkout" value="checkout" />
	</form>
</body>
</html>