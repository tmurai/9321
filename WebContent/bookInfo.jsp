<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Book Information</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="pagehdr">
			<h1>Food for Thought Bookstore</h1>
		</div>
			Title: ${book.title} <br/>
			Author(s): ${book.author} <br/>
			Genre(s): ${book.genre} <br/>
			Year: ${book.year} <br/>
			Series: ${book.series} <br/>
			Edition: ${book.edition} <br/>
			Publisher: ${book.publisher} <br/>
			Price: <fmt:formatNumber type="number" minFractionDigits="2"
					maxFractionDigits="2" value="${book.price}" /> <br/>

		<FORM Action="searchResult.jsp" METHOD='POST'>
			<INPUT TYPE="submit" VALUE="Back to search results">
		</form>
	</body>
</html>