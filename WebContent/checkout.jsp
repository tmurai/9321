<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!--
    Assign 1 instructions:
    
    This page displays the final shopping cart. User can confirm the purchase (yes or no)
    
    If yes, the user is taken to a confirmation page (confirm.jsp) that displays the total purchase price
    If no, the user is taken back to the search page.
    
    In both cases, the session is invalidated.   
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Confirm your purchase</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="pagehdr">
			<h1>Food for Thought Bookstore</h1>
		</div>
		<H2> This is your final shopping cart: </H2>
		<table border="0">
		  <tr>
		    <th>Item</th>
		    <th>Price</th>
		  </tr>
		  <c:if test="${cart.books != null}">
				<c:forEach var="book" items="${cart.books}">
					<tr>
						<td>${book.title}</td>
						<td><fmt:formatNumber type="number" 
							minFractionDigits="2" maxFractionDigits="2" 
							value="${book.price}" /></td>
					</tr>
				</c:forEach>
			</c:if>
		  <tr>
		  	<td>Total Cost</td>
		  	<td><fmt:formatNumber type="number" 
							minFractionDigits="2" maxFractionDigits="2" 
							value="${cart.sum}" /></td>
		  </tr>
		</table>
		Would you like to confirm your purchase? <br/>
		 <form name="confirmYes" action="controller?op=checkout" method="POST">
	    		<input type="submit" name="checkout_yes" value="Yes">
	    		<input type="submit" name="checkout_no" value="No">
		</form>
	</body>
</html>