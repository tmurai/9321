<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Checkout</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div id="pagehdr">
		<h1>Food for Thought Bookstore</h1>
	</div>
	<H3> Your purchase is confirmed </H3>
	<H4> Total price: <fmt:formatNumber type="number" 
							minFractionDigits="2" maxFractionDigits="2" 
							value="${cart.sum}" /></H4>
	<br/>
	Go back to the <a href="controller?op=welcome">home page</a>
	</body>
</html>