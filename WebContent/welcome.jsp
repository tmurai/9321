<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!--
	Assign 1 instructions:
	This is a landing page for the website and provides a link to the search page. 
	There is no need to modify this page. 
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Food for Thought</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="pagehdr">
			<h1>Food for Thought Bookstore</h1>
		</div>
		<h2>Welcome!</h2> <br></br>
		Please <a href="controller?op=callSearch">click here</a> to search books
	</body>
</html>