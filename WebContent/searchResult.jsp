<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--
	Assign 1 instructions:
	This page displays the list of books that match the user's search criteria.
	For each book, its title and a checkbox is displayed. The title is a link to 
	a separate page, bookInfo.jsp that displays the detailed information about the book
	The link to bookInfo.jsp must be mediated via the controller and must follow the format
	shown in the example in the comment below.
	
	If no books were found during search, then the page prints "No books found".
	
	User selects all the books that he/she is interested in and adds them to the cart by 
	hitting the submit button. This takes the user to reviewCart.jsp  
 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- jsp:useBean id="searchList" class="edu.unsw.comp9321.bookstore.BookList" scope="session"/> -->
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
		<title>Search Result</title>
		<link href="CSS.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="pagehdr">
			<h1>Food for Thought Bookstore</h1>
		</div>
	<c:if test="${searchList.books == null}">
		<div id="body">
			<p>No books found</p>
			Please <a href="controller?op=callSearch">click here</a> to search for books.
		</div>
	</c:if>
	<c:if test="${searchList.books != null}">
		<form name="searchResult" action="controller?op=addCart" method="POST">
		<c:forEach var="book" items="${searchList.books}">
			<c:url value="controller?op=info" var="url">
				<c:param name="title" value="${book.title}"/>
			</c:url>
			<a href="${url}">${book.title}</a>
			<input type="checkbox" name="bookCheckBox" value="${book.title}"/>
			<br />
		</c:forEach>
			<input type="submit" name="addCart" title="Add to cart"
				value="Add to Cart">
		</form>
	</c:if>

	<!--
		Example of an item:
		  
		<a href="controller?op=info&{Your Parameters Here}">The Ancient Roman World</a>
						<input type="checkbox" name="option" value={Your Value Here}/> <br />
		-->
		
	</body>
</html>